import React, { useState } from 'react';
import { APIS } from '../utils/api-factory'
import { useServerData } from '../state/serverDataContext';
import { YEARS } from '../utils/constant'
const fetch = require("node-fetch");

const SpaceX = () => {
    const filterHandler = (e, type, val) => {
        e.preventDefault();
        if (type === 'year') {
            setYearFilter(val)
            filteredData(val, launchFilter, landFilter)
        } else if(type === 'launch'){
            setLaunchFilter(val)
            filteredData(yearFilter, val, landFilter)
        } else if(type === 'land'){
            setLandFilter(val)
            filteredData(yearFilter, launchFilter, val)
        } else {
            setYearFilter('')
            setLaunchFilter('')
            setLandFilter('')
            filteredData('', '', '')
        }
    }

    const filteredData = (year, launch, land) => {
        fetch(APIS.ALL_DATA + '&launch_success=' + launch + '&land_success=' + land + '&launch_year=' + year)
            .then(res => res.json())
            .then(missions => {
                setMission(missions)
            })
    }

    const serverMissions = useServerData(data => {
        return data.missions || [];
    });
    const [missions, setMission] = useState(serverMissions);
    const [yearFilter, setYearFilter] = useState('')
    const [launchFilter, setLaunchFilter] = useState('')
    const [landFilter, setLandFilter] = useState('')
    return (
        <>
            <div className="main-heading">SpaceX Launch Program</div>
            <div className="mission-main-container">
                <div className="mission-filters">
                    <h3 className="heading">Filters</h3>
                    <div className="filter-head">Launch Year</div>
                    <ul className="year-list">
                        {YEARS.map(year => {
                            return (
                                <li key={year}>
                                    <button
                                        className="filter-btn"
                                        onClick={e => filterHandler(e, 'year', year)}
                                    >
                                        {year}
                                    </button>
                                </li>
                            )
                        })}
                    </ul>
                    <div className="filter-head mt-20px">Successful Launch</div>
                    <ul className="year-list">
                        <li>
                            <button
                                className="filter-btn"
                                onClick={e => filterHandler(e, 'launch', 'true')}
                            >True
                            </button>
                        </li>
                        <li>
                            <button
                                className="filter-btn"
                                onClick={e => filterHandler(e, 'launch', 'false')}
                            >False
                            </button>
                        </li>
                    </ul>
                    <div className="filter-head mt-20px">Successful Landing</div>
                    <ul className="year-list">
                        <li>
                            <button
                                className="filter-btn"
                                onClick={e => filterHandler(e, 'land', 'true')}
                            >True
                            </button>
                        </li>
                        <li>
                            <button
                                className="filter-btn"
                                onClick={e => filterHandler(e, 'land', 'false')}
                            >False
                            </button>
                        </li>
                    </ul>
                    <button className="clear-filter" onClick={e => filterHandler(e, '', '')}>Clear Filter</button>
                </div>
                <ul className="mission-cards-list">
                    {missions.length > 0 ?
                    missions.map(mission => (
                        <li className="" key={mission.flight_number}>
                            <img src={mission.links.mission_patch_small} alt="" />
                            <div className="mission-detail">
                                <p className="blue-color">
                                    {mission.mission_name + ' #' + mission.flight_number}
                                </p>
                                {mission.mission_id.length > 0 ?
                                    <>
                                        <b className="">Mission Ids:</b>
                                        <ul className="id-list">
                                            {mission.mission_id.map(id => {
                                                return (
                                                    <li key={id}>{id}</li>
                                                )
                                            })}
                                        </ul>
                                    </>
                                    : null}
                                <p className="">
                                    <b>Launch Year: </b>
                                    <span className="blue-color">{mission.launch_year}</span>
                                </p>
                                <p className="">
                                    <b>Successful Launch: </b>
                                    <span className="blue-color">{mission.launch_success ? 'Yes' : 'No'}</span>
                                </p>
                                <p className="">
                                    <b>Successful Landing: </b>
                                    <span className="blue-color">{mission.launch_landing ? 'Yes' : 'No'}</span>
                                </p>
                            </div>
                        </li>
                    ))
                : <div className="no-data">No data found.</div>}
                </ul>
            </div>
        </>
    );
};

SpaceX.fetchData = () => {
    return fetch(APIS.ALL_DATA)
        .then(res => res.json())
        .then(missions => {
            return {
                missions
            };
        })
};

export default SpaceX;
