import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Loadable from 'react-loadable';

import Head from './Head';

const LoadableSpaceX = Loadable({
  loader: () => import(/* webpackChunkName: 'SpaceX' */ './SpaceX'),
  loading: () => <div>Loading...</div>
});

const App = () => (
  <>
    <Head />

    <Switch>
      <Route exact path="/" component={LoadableSpaceX} />
    </Switch>
  </>
);

export default App;
