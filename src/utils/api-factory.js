const APIS = {
    ALL_DATA: 'https://api.spaceXdata.com/v3/launches?limit=100',
    LAUNCH_FILTER: 'https://api.spaceXdata.com/v3/launches?limit=100&launch_success=true',
    LAUNCH_LAND_FILTER: 'https://api.spaceXdata.com/v3/launches?limit=100&launch_success=true&land_success=true',
    LAUNCH_LAND_YEAR_FILTER: 'https://api.spaceXdata.com/v3/launches?limit=100&launch_success=true&land_success=true&launch_year=2014'
}

export {
    APIS
}